package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    public CannotBuildPyramidException() {}

    public CannotBuildPyramidException(String message, Throwable e)
    {
        super(message, e);
    }
}
