package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int n = 1;
        Integer[][] result;
        try {
            if (inputNumbers.contains(null)) {
                throw new CannotBuildPyramidException();
            }
            Collections.sort(inputNumbers);
            Collections.reverse(inputNumbers);

            int s = 1;
            while (s < inputNumbers.size()) {
                s = n*(n + 1)/2;
                n++;
            }
            if (s > inputNumbers.size()){
                throw new CannotBuildPyramidException();
            }

            s = 2*(n - 1) - 1;
            result = new Integer[n - 1][s];

            for (Integer[] line: result) {
                Arrays.fill(line, 0);
            }

            Iterator<Integer> it = inputNumbers.iterator();
            for (int i = 0; i <= s / 2; i++) {
                if (i == s / 2) {
                    result[0][s / 2] = it.next();
                } else {
                    for (int j = s - 1 - i; j >= i; j -= 2) {
                        result[n - 2 - i][j] = it.next();
                    }
                }
            }

        } catch (CannotBuildPyramidException e){
            throw new CannotBuildPyramidException("Unable to build a pyramid", e);
        }
        return result;
    }


}
