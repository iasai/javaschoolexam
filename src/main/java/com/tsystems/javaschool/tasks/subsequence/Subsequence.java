package com.tsystems.javaschool.tasks.subsequence;

import java.util.HashMap;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        HashMap<Object, Integer> searchant = new HashMap<>();
        int i = 0;
        for (Object o: y){
            searchant.put(o, i);
            i++;
        }
        int prev = -1;
        for (Object o_x: x){
            if (searchant.containsKey(o_x)) {
                Integer temp = searchant.get(o_x);
                if (temp > prev) {
                    prev = temp;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }
}
