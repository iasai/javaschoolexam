package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class MyStack {

    private ArrayList<Token> tokens;

    public MyStack() {
        this.tokens = new ArrayList<>();
    }

    public boolean isEmpty() {
        return this.tokens.size() == 0;
    }

    public Token peek() {
        return this.tokens.get(this.tokens.size() - 1);
    }

    public void push(Token t) {
        this.tokens.add(t);
    }

    public void pop() {
        this.tokens.remove(this.tokens.size() - 1);
    }


}