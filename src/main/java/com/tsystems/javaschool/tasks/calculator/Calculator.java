package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

public class Calculator {

    private MyStack operatorStack;
    private MyStack operandStack;
    private boolean invalid_input = false;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String res = _evaluate(tokenize(statement));
        if (res == null) { return null;}
        else {
            double temp = Double.parseDouble(res);
            Locale.setDefault(Locale.US);
            DecimalFormat df = new DecimalFormat("#.####");
            df.setRoundingMode(RoundingMode.CEILING);
            df.setDecimalSeparatorAlwaysShown(false);
            return String.valueOf(df.format(temp));
        }
        return "";
    }

    private String _evaluate(ArrayList<Token> tokens) {
        for (Token nextToken: tokens){
            switch (nextToken.getType()) {
                case "OPERAND":
                    operandStack.push(nextToken);
                    break;
                case "OPERATOR":
                    if (operatorStack.isEmpty() || nextToken.getPrecedence() > operatorStack.peek().getPrecedence()) {
                        operatorStack.push(nextToken);
                    } else {
                        while (!operatorStack.isEmpty() && nextToken.getPrecedence() <= operatorStack.peek().getPrecedence()) {
                            Token t = operatorStack.peek();
                            operatorStack.pop();
                            applyOperator(t);
                        }
                        operatorStack.push(nextToken);
                    }
                    break;
                case "L_P":
                    operatorStack.push(nextToken);
                    break;
                case "R_P":
                    while (!operatorStack.isEmpty() && operatorStack.peek().getType().equals("OPERATOR")) {
                        Token t = operatorStack.peek();
                        operatorStack.pop();
                        applyOperator(t);
                    }
                    if (!operatorStack.isEmpty() && operatorStack.peek().getType().equals("L_P")) {
                        operatorStack.pop();
                    } else {
                        invalid_input = true;
                    }
                    break;
                case "UNIDENTIFIED":
                    invalid_input = true;
                    break;
            }
        }
        while (!operatorStack.isEmpty() && operatorStack.peek().getType().equals("OPERATOR")) {
            Token t = operatorStack.peek();
            operatorStack.pop();
            applyOperator(t);
        }

        if (invalid_input) { return null; }
        else {
            Token result = operandStack.peek();
            operandStack.pop();
            if (!operatorStack.isEmpty() || !operandStack.isEmpty()) { return null; }
            else return String.valueOf(result.getValue());
        }
    }

    /**
     * Converts input string into a list of tokens
     *
     * @param input_string mathematical statement
     *
     * @return a list of tokens
     */

    private ArrayList<Token> tokenize(String input_string){
        ArrayList<Token> tokens = new ArrayList<>();
        char prev = ' ';
        String number = "";
        int n = input_string.length();
        for (int i = 0; i < n; i++){
            if (Character.isDigit(input_string.charAt(i)) && i == n - 1) {
                number += input_string.charAt(i);
                tokens.add(new Token(number));
            } else if (Character.isDigit(input_string.charAt(i)) || (input_string.charAt(i) == '.' && Character.isDigit(prev))){
                number += input_string.charAt(i);
                prev = input_string.charAt(i);
            } else if (!Character.isDigit(input_string.charAt(i)) && Character.isDigit(prev)){
                tokens.add(new Token(number));
                tokens.add(new Token(String.valueOf(input_string.charAt(i))));
                prev = input_string.charAt(i);
                number = "";
            } else {
                tokens.add(new Token(String.valueOf(input_string.charAt(i))));
                prev = input_string.charAt(i);
            }
        }
        return tokens;
    }

    /**
     * Applies an operator token to two operand tokens
     *
     * @param operator token
     *
     */
    private void applyOperator(Token t) {
        Token value_1 = null;
        Token value_2 = null;
        double result = 0;
        if (operandStack.isEmpty()) {
            invalid_input = true;
            return;
        } else {
            value_2 = operandStack.peek();
            operandStack.pop();
        }
        if (operandStack.isEmpty()) {
            invalid_input = true;
            return;
        } else {
            value_1 = operandStack.peek();
            operandStack.pop();
        }
        switch(t.getOperator()) {
            case '+':
                result = value_1.getValue() + value_2.getValue();
                break;
            case '-':
                result = value_1.getValue() - value_2.getValue();
                break;
            case '*':
                result = value_1.getValue() * value_2.getValue();
                break;
            case '/':
                if (value_2.getValue() == 0) {
                    invalid_input = true;
                    break;
                }
                result = value_1.getValue() / value_2.getValue();
                break;
        }
        Token temp = new Token(result);
        operandStack.push(temp);
    }

}
