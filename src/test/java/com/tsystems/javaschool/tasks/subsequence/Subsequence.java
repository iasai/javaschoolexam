import java.util.HashMap;
import java.util.List;

public class Subsequence {

    public boolean find(List<Object> x, List<Object> y){
        HashMap<Object, Integer> searchant = new HashMap<>();
        int i = 0;
        for (Object o: y){
            searchant.put(o, i);
            i++;
        }
        int prev = -1;
        for (Object o_x: x){
            if (searchant.containsKey(o_x)) {
                Integer temp = searchant.get(o_x);
                if (temp > prev) {
                    prev = temp;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

}
