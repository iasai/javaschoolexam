public class Token {

    private String type;
    private int precedence;
    private char operator;
    private double value;

    public Token(String input_string){
        switch (input_string){
            case "+":
                type = "OPERATOR";
                precedence = 1;
                operator = input_string.charAt(0);
                break;
            case "-":
                type = "OPERATOR";
                precedence = 1;
                operator = input_string.charAt(0);
                break;
            case "*":
                type = "OPERATOR";
                precedence = 2;
                operator = input_string.charAt(0);
                break;
            case "/":
                type = "OPERATOR";
                precedence = 2;
                operator = input_string.charAt(0);
                break;
            case "(":
                type = "L_P";
                break;
            case ")":
                type = "R_P";
                break;
            default:
                type = "OPERAND";
                try {
                    value = Double.parseDouble(input_string);
                } catch (Exception e){
                    type = "UNIDENTIFIED";
                }
        }
    }

    public Token(double val){
        type = "OPERAND";
        value = val;
    }

    public String getType() {
        return this.type;
    }

    public int getPrecedence(){
        return this.precedence;
    }

    public char getOperator() {
        return this.operator;
    }

    public double getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        if (this.type.equals("OPERAND")){ return String.valueOf(this.value); }
        else if (this.type.equals("OPERATOR")){ return String.valueOf(this.operator); }
        else if (this.type.equals("L_P")) { return "("; }
        else if (this.type.equals("R_P")) { return ")"; }
        return null;
    }
}