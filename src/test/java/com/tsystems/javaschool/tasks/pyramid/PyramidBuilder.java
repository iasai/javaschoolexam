import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    class CannotBuildPyramidException extends RuntimeException
    {
        public CannotBuildPyramidException() {}

        public CannotBuildPyramidException(String message, Throwable e)
        {
            super(message, e);
        }
    }

    public Integer[][] buildPyramid(List<Integer> input){
        int n = 1;
        Integer[][] result;
        try {
            if (input.contains(null)) {
                throw new CannotBuildPyramidException();
            }
            Collections.sort(input);
            Collections.reverse(input);

            int s = 1;
            while (s < input.size()) {
                s = n*(n + 1)/2;
                n++;
            }
            if (s > input.size()){
                throw new CannotBuildPyramidException();
            }

            s = 2*(n - 1) - 1;
            result = new Integer[n - 1][s];

            for (Integer[] line: result) {
                Arrays.fill(line, 0);
            }

            Iterator<Integer> it = input.iterator();
            for (int i = 0; i <= s / 2; i++) {
                if (i == s / 2) {
                    result[0][s / 2] = it.next();
                } else {
                    for (int j = s - 1 - i; j >= i; j -= 2) {
                        result[n - 2 - i][j] = it.next();
                    }
                }
            }

        } catch (CannotBuildPyramidException e){
            throw new CannotBuildPyramidException("Unable to build a pyramid", e);
        }
        return result;
    }

}